package com.bontemps.mytodolist.ui.pages.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bontemps.mytodolist.R;

public class MainActivity extends AppCompatActivity {
    private MainActivityViewModel viewModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        setListView();
    }

    private void setListView() {
        ListView listView = (ListView) findViewById(R.id.list);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);

        viewModel.getTodoList().observe(this, todoList -> {
            BaseAdapter adapter = new MyAdapter(this,
                    todoList.toArray(new String[0]));
            listView.setAdapter(adapter);
            progressBar.setVisibility(View.GONE);
        });
    }
}